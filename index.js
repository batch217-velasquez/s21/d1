// console.log("Hello World")

// [SECTION] Arrays
// Array are used to store multiple related values in a single variable
// Usually we declare an array using square brackets ([]) also knowns "Array Literals"

let studentNameA = '2020-1923';
let studentNameB = '2020-1924';
let studentNameC = '2020-1925';
let studentNameD = '2020-1926';

// Declaration of an array
// Syntax --> let/con arrayName = [elementA, elementB, elementC, ......]

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926'];

// Common example of arrays
let grades = [98.5, 98.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu" ];

let mixedArray = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArray); 


// Alternative way of writing an array

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
]

// Create an array with values from variable
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);


// [SECTION] Length Property
// The lenght property allows us to get and set the total number of items in an array.

let fullName = "Jamie Noble"; 
console.log(fullName.length);

console.log(cities.length);

// legnth properry can also set the total number of items....



// We use .legnth-1 to delete the last item in an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// ......


// Another example using decrementation (--)
// Another way to delete item on an array
cities.length--;
console.log(cities);

// We can't do same on strings, however.
fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

// ....
// Adding an item in an array using incrementation (++)
let theBeatles = ["John", "Paul", "Ringo", "Geprge"]
theBeatles.length++;
theBeatles.length++;
theBeatles.length++;
console.log(theBeatles);


// [SECTION] Reading from Arrays
// Accesing array elements is one of the more common tasks that we do with an array.

// We can access a specific data in array using an index
// Each element in an array is associated with its own index/number
console.log(grades[0]);
console.log(computerBrands[3]);

console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegends[1]);
console.log(lakersLegends[4]);
console.log(lakersLegends[0]);

// You can also save/store array items in another variable:

let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also reassign array using the items indices
console.log("Array before re-assignement");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after re-assignemnet");
console.log(lakersLegends);

// Accesing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kucoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

// You can also add it directyly
console.log(bullsLegends[bullsLegends.length-1]);

// Adding items to an array
// Using indices, you can also add items into teh array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tiffa Lockhart"
console.log(newArr);

// ......

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

// Looping over an Array
// You can use a for loop to iterate over all items in an array
// Set the counter as the index and set the condition that as long as the current index iterated is less than the length of the array.

for (let index = 0; index < newArr.length; index++){
	// index = 0 < 3 --> T --> "Cloud Strife"
	// index = 1 < 3 --> T --> "Tifa Lockhart"
	// index = 2 < 3 --> T --> "Barret Wallace"
	// index = 3 < 3 --> False --> break/stop for loops
	// You can use the loop counter as index to be able to show each arrat items in the console
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40,];
numArr[numArr.length] = 100;
numArr[numArr.length] = 87;
numArr[numArr.length] = 90;

for (let index = 0; index < numArr.length; index++){


	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}

	else {
		console.log(numArr[index] + " is not divisble by 5")


	}
}


// [SECTION] Multidimensional Array

// ......


// 8x8 dimension
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

// [1] --> column
// [4] --> row
console.log(chessBoard[1][4]);

console.log(chessBoard[4][3]);

console.table(chessBoard);

console.log("Pawn moves to: " + chessBoard[1][5]);


